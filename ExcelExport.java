import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.io.*;

public class ExcelExport {
  public static class Data {
    public String id;
    public String name;
    public String description;

    public String output() {
      return this.id + ';' + this.name + ';' + this.description + "\r\n";
    }
  };

  public static void writeToFile(int i, ArrayList<Data> dataList) {
    try {
      System.out.println(i);
      FileOutputStream fileOut = new FileOutputStream("data.csv", true);
      PrintStream out = new PrintStream(fileOut);
      for (Data dataListItem : dataList) {
        out.print(dataListItem.output());
      }
      out.close();
      fileOut.close();
      dataList.clear();
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public static void main(String args[]) {
    System.out.println("Program start");

    try {
      Class.forName("org.postgresql.Driver");
      Connection connection = DriverManager
        .getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "system");
      System.out.println("Opened database successfully");

      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM FOO, BAR;");

      ArrayList<Data> dataList = new ArrayList<Data>();
      int i = 0;
      while (resultSet.next()) {
        if ((i % 10000) == 0 ) {
          writeToFile(i, dataList);
        }
        Data data = new Data();
        data.id = resultSet.getString("id");
        data.name = resultSet.getString("name");
        data.description = resultSet.getString("description");
        dataList.add(data);
        i++;
      }

      writeToFile(i, dataList);

      System.out.println("Query executed");
      resultSet.close();
      statement.close();
      connection.close();
      System.out.println("Connection closed - bye");
    } catch (Exception e) {
      System.out.println("Someting went wrong");
      System.out.println(e);
      System.exit(0);
    }
  }
}
