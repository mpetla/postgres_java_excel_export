create table foo
(
	id uuid not null primary key,
  name varchar(255),
  description text
);

create table bar
(
	id uuid not null primary key,
  name varchar(255),
  description text
);

do
$$
begin
	for i in 1..1500 loop
		insert into foo(select gen_random_uuid(), 'foo ' || gen_random_uuid(), 'description ' || gen_random_uuid());
    insert into bar(select gen_random_uuid(), 'bar ' || gen_random_uuid(), 'description ' || gen_random_uuid());
  end loop;
end;
$$

select  count(1)
from    foo f,
		    bar b;
